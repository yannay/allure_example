
import allure
import pytest

def get_order_json_without_barcodes():
    return {
        "partnerOrders": [
            {
                "cargoes": [
                    {
                        "currency": "RUB",
                        "height": 100,
                        "length": 100,
                        "price": 100,
                        "productValues": [
                            {
                                "barcode": "3292543220987654432",
                                "codeGTD": "codeGTD",
                                "codeTNVED": "codeTNVED",
                                "currency": "RUB",
                                "name": "Samsung Galaxy S20 Ultra черный",
                                "originCountry": "US",
                                "price": 100,
                                "value": 1,
                                "vat": 0,
                                "vendorCode": "10293029400"
                            },
                            {
                                "barcode": "32287665481228",
                                "codeGTD": "codeGTD",
                                "codeTNVED": "codeTNVED",
                                "currency": "RUB",
                                "name": " Смартфон Apple iPhone 11 Pro Max 256 gb черный",
                                "originCountry": "US",
                                "price": 100,
                                "value": 1,
                                "vat": 0,
                                "vendorCode": "10293029400"
                            }
                        ]
                    }
                ]
            }
        ]
    }


barcode_gen = {
    'disabled': 'DISABLED',
    'enabled': 'ENABLED',
    'partial': 'PARTIAL'
}

barcode = {
    'Полностью не заполнен в одном заказе': get_order_json_without_barcodes(),  # удалены баркоды грузов
    'Полностью заполнен в одном заказе': 'updated',  # баркоды  в двух грузах
    'Заполнен частично в 1 заказе': 'updated_once',  # в одном баркод есть во втором нет
    'Заполнен частично в пачке заказов': 'updated_all'  # в 1 заказе есть, во 2 нет.
}

test_data = [
    (barcode_gen['partial'], 'v3', 400, barcode['Заполнен частично в пачке заказов']),
    (barcode_gen['partial'], 'v3', 400, barcode['Заполнен частично в 1 заказе']),
    (barcode_gen['disabled'], 'v3', 400, barcode['Заполнен частично в пачке заказов']),
    (barcode_gen['disabled'], 'v3', 400, barcode['Заполнен частично в 1 заказе']),
    (barcode_gen['disabled'], 'v3', 200, barcode['Полностью заполнен в одном заказе']),

    (barcode_gen['enabled'], 'v3', 200, barcode['Полностью не заполнен в одном заказе']),
    (barcode_gen['disabled'], 'v2', 400, barcode['Заполнен частично в пачке заказов']),
    (barcode_gen['partial'], 'v1', 400, barcode['Заполнен частично в 1 заказе']),
    (barcode_gen['disabled'], 'v1', 200, barcode['Полностью заполнен в одном заказе']),
    (barcode_gen['partial'], 'v2', 400, barcode['Заполнен частично в пачке заказов']),

    (barcode_gen['disabled'], 'v1', 400, barcode['Заполнен частично в 1 заказе']),
    (barcode_gen['enabled'], 'v2', 400, barcode['Заполнен частично в 1 заказе']),
    (barcode_gen['disabled'], 'v2', 400, barcode['Полностью не заполнен в одном заказе']),
    (barcode_gen['enabled'], 'v1', 400, barcode['Полностью не заполнен в одном заказе']),
    (barcode_gen['partial'], 'v2', 400, barcode['Полностью не заполнен в одном заказе']),

    (barcode_gen['disabled'], 'v2', 200, barcode['Полностью заполнен в одном заказе']),
    (barcode_gen['enabled'], 'v1', 400, barcode['Заполнен частично в пачке заказов']),
    (barcode_gen['disabled'], 'v3', 400, barcode['Полностью не заполнен в одном заказе']),
    (barcode_gen['partial'], 'v3', 200, barcode['Полностью заполнен в одном заказе']),
    (barcode_gen['enabled'], 'v3', 400, barcode['Полностью заполнен в одном заказе']),

    (barcode_gen['enabled'], 'v3', 400, barcode['Заполнен частично в пачке заказов']),
    (barcode_gen['enabled'], 'v3', 400, barcode['Заполнен частично в 1 заказе'])
]

@allure.suite('Создание заказа')
@allure.label('subSuite', 'oApi')
@allure.id('61710')
class Test61710:
    @pytest.mark.b2br
    @pytest.mark.parametrize("barcode_gen,version,status,barcode", test_data)
    @allure.title('61710 - Автоматическая генерация поля cargoes.barcodes.value при создании заказа')
    def test_b2br_61710_oapi_create_order_with_auto_generation_barcodes(self, barcode_gen, version, status,
                                                                        barcode):

        print(test_data)
        assert 1 == 1 